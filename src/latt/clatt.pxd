cdef extern from "latt.h":
    const char* __version__ "LATT_VERSION"
    const char* __description__ "LATT_SHORTDESC"
    const char* __copyright__ "LATT_COPYRIGHT"
    const char* __maintainer__ "LATT_MAINTAINER"
    const char* __license__ "LATT_LICENSE"
    const char* __author__ "LATT_AUTHOR"
    