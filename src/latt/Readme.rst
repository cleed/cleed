=====
Latt
=====

``latt`` is a supplementary program for the CLEED package and is used to quickly generate 
a large substrate surface.

For help with how to use ``latt`` type:

    latt --help
    
Bugs
====

A list of known bugs are:

#. Malloc failure for type 'coord_t' on Windows using MinGW