/*********************************************************************
 *                      BulkModel.hh
 *
 *  Copyright 2013-2014 Liam Deacon <liam.m.deacon@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later.
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 *
 *********************************************************************/

/*! \file
 *
 * Provides a CLEED bulk model class.
 */

#ifndef __BulkModel_hh__
#define __BulkModel_hh__

#ifdef __cplusplus /* use C linkage if this is a C++ compiler */

#include <Model.hh>
using namespace std;

namespace cleed {

class BulkModel : Model {
  public:

    /* getters */

    /* setters */

  private:

};

} /* namespace CLEED */

#endif /* __cplusplus */

#endif /* __BulkModel_hh__ */
