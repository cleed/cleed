/*********************************************************************
 *                          RoIRFactor.h
 *
 *  Copyright (C) 2013-2015 Liam Deacon <liam.m.deacon@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later.
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 *
 * Changes:
 *   LD/03.01.2015 - creation
 *********************************************************************/

/*!
 * \file Rfactor.h
 * \author Liam Deacon
 *
 * Angle of Incidence wrapper for RFactor class.
 */


#ifndef AOIRFACTOR_H
#define AOIRFACTOR_H

#ifdef __cplusplus

#include <RFactor/RFactor.hh>

namespace cleed {
//!TODO

}

#endif /* __cplusplus */

#endif /* AOIRFACTOR_H
