/*********************************************************************
 *                      LEED.hh
 *
 *  Copyright 2013-2014 Liam Deacon <liam.m.deacon@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later.
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 *
 *********************************************************************/

/*! \file
 *
 * C++ wrapper class for leed structure.
 */

#ifndef __LEED_HH__
#define __LEED_HH__

#ifdef __cplusplus /* use C linkage if this is a C++ compiler */

#include "leed_def.h"

namespace cleed {

class LEED {
  public:
    LEED();
    ~LEED();

  private:

};

} /* namespace CLEED */

#endif /* __cplusplus */

#endif /* __LEED_HH__ */
