/*********************************************************************
 *                      Model.h
 *
 *  Copyright 2013-2014 Liam Deacon <liam.m.deacon@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later.
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 *
 *********************************************************************/

/*! \file
 *
 * Provides a base class for CLEED atomic models.
 */

#ifndef __MODEL_HH__
#define __MODEL_HH__

#include <Core/Atom.hh>

#ifdef __cplusplus /* use C linkage if this is a C++ compiler */

using namespace std;

namespace cleed {

class Model {
  public:

    /* getters */

    /* setters */

  private:

};

} /* namespace CLEED */

#endif /* __cplusplus */

#endif /* __MODEL_HH__ */
