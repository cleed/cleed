/*********************************************************************
 *                      SRGA.C
 *
 *  Copyright 1992-2014 Georg Held <g.held@reading.ac.uk>
 *
 *  Licensed under GNU General Public License 3.0 or later.
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 *
 * Changes:
 *   LD/02.02.2015 - Creation (copy from srsx.c)
 ***********************************************************************/

#include "search.h"

/*! \file
 *
 * Perform a search according to the GENETIC ALGORITHM METHOD.
 */
void sr_ga(size_t n_dim, real dpos, const char *bak_file, const char *log_file)
{
  ERROR_MSG("Genetic Algorithm not yet implemented\n");
  exit(SR_SEARCH_NOT_IMPLEMENTED);
} /* end of function sr_sa */

