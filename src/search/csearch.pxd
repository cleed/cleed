cdef extern from "search.h":
    const char* __version__ "SEARCH_VERSION"
    const char* __description__ "SEARCH_SHORTDESC"
    const char* __copyright__ "SEARCH_COPYRIGHT"
    const char* __maintainer__ "SEARCH_MAINTAINER"
    const char* __license__ "SEARCH_LICENSE"
    const char* __author__ "SEARCH_AUTHOR"
    