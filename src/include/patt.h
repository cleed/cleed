/*********************************************************************
 *                           PATT.H
 *
 *  Copyright 1992-2014 Georg Held <g.held@reading.ac.uk>
 *  Copyright 2014 Liam Deacon <liam.m.deacon@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later.
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 *
 * Changes:
 *   LD/2014.08.23 - header created by split from patt.c
 *********************************************************************/

/*!
 * \file
 * \brief Master include for \c patt - LEED pattern visualisation program.
 *
 * \c patt simulates Low Energy Electron Diffraction (LEED) patterns
 * to help in analysis of complex surface structures. Diffraction peaks
 * (spots), indices, reciprocal lattice vectors & different domains can
 * all be visualised using vector graphics (e.g. PostScript).
 *
 */

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
extern "C" {
#endif

#ifndef _PATT_H
#define _PATT_H

#include <stdio.h>

#include "patt_ver.h"
#include "patt_def.h"
#include "patt_func.h"

#endif /* _PATT_H */

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
}
/* add C++ features here */


#endif
