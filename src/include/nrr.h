/************************************************************************
 * <FILENAME>
 *
 *  Copyright 1992-2014 Georg Held <g.held@reading.ac.uk>
 *
 *  Licensed under GNU General Public License 3.0 or later.
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 *
 * Changes:
 *20.09.95

include file for real Num. Rec. routines.
*********************************************************************/
#ifndef NRR_H
#define NRR_H

#include "cleed_real.h"
#include "nrr_util.h"
#include "nrr_func.h"

#endif /* NRR_H */
