/*********************************************************************
Num. Rec. functions
*********************************************************************/

real brent(real , real , real , real(*)(real), real , real *);
real f1dim(real );
void linmin(real *, real *, size_t , real *, real (*)() );
void mnbrak(real *, real *, real *, real *, real *, real *, real (*)(real) );
real ran1(long *);
