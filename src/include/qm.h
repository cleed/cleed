/*********************************************************************
 *                           QM.H
 *
 *  Copyright 1994-2014 Georg Held <g.held@reading.ac.uk>
 *
 *  Licensed under GNU General Public License 3.0 or later.
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 *
 *
 * Description: master include for basic quantum mechanical functions
 *  - type definitions
 *  - constant values
 *
 * Changes:
 *   GH/1994.06.06 - creation
 *********************************************************************/

#ifndef QM_H
#define QM_H

/*********************************************************************
 * Other includes
 *********************************************************************/
#include "gh_stddef.h"
#include "cleed_real.h"
#include "cpl.h"
#include "mat.h"
#include "qm_func.h"

#endif /* QM_H */
