/*********************************************************************
 *													cleed_types.h
 *
 *  Copyright (C) 2013-2015 Liam Deacon <liam.m.deacon@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 *
 * Changes:
 *   LD/29.01.2015 - creation
 *********************************************************************/

/*!
 * \file
 * \author Liam Deacon
 *
 * This file defines the standard data types for CLEED, providing a unified
 * interface through an abstract factory design pattern.
 */


#ifndef CLEED_TYPES_H
#define CLEED_TYPES_H

#include "cleed_real.h"
#include "cleed_complex.h"
#include "cleed_vector.h"
#include "cleed_matrix.h"

#endif /* CLEED_TYPES_H */
