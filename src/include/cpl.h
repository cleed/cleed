/*********************************************************************
 *                           CPL.H
 *
 *  Copyright 1994-2014 Georg Held <g.held@reading.ac.uk>
 *
 *  Licensed under GNU General Public License 3.0 or later.
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 *
 * Changes:
 *   GH/1994.06.06 - creation
 *
 ********************************************************************/

/*!
 * \file
 * \author Georg Held <g.held@reading.ac.uk>
 * \brief  Master header file for basic complex functions.
 */

#ifndef CPL_H
#define CPL_H

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
extern "C" {
#endif

#include <math.h>

/*********************************************************************
 * Other includes
 *********************************************************************/
#include "cleed_real.h"
#include "cpl_macr.h"
#include "cpl_func.h"

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
}
#endif

#endif /* CPL_H */
