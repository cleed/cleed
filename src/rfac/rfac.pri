QMAKE_CFLAGS += -std=gnu11 -Wall -Wextra -pedantic

RFAC_SRCS = $$PWD/bgets.c \
            $$PWD/file2buffer.c \
            $$PWD/rfac.c \
            $$PWD/rfac_ivcur.c \
            $$PWD/rfhelp.c \
            $$PWD/rfinput.c \
            $$PWD/rfintindl.c \
            $$PWD/rflines.c \
            $$PWD/rflorentz.c \
            $$PWD/rfmklide.c \
            $$PWD/rfmklist.c \
            $$PWD/rfr1.c \
            $$PWD/rfr2.c \
            $$PWD/rfrb.c \
            $$PWD/rfrdargs.c \
            $$PWD/rfrdcleed.c \
            $$PWD/rfrdexpt.c \
            $$PWD/rfrmin.c \
            $$PWD/rfrp.c \
            $$PWD/rfsort.c \
            $$PWD/rfspline.c \
            $$PWD/rfsplint.c \
            $$PWD/rfversion.c
