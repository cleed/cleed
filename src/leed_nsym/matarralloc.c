/*********************************************************************
 *                        MATARRALLOC.C
 *
 *  Copyright 1992-2014 Georg Held <g.held@reading.ac.uk>
 *
 *  Licensed under GNU General Public License 3.0 or later.
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 *
 * Changes:
 *   GH/20.01.95 - Creation (copy from matalloc)
 *********************************************************************/

/*! \file
 *
 * Implements matarralloc() function to allocate an array of matrices
 * memory for a cols x rows matrix.
 */

#include <stdio.h>
#include <stdlib.h>

#include "mat.h"

/*!
 * Allocates memory for an array of matrices. Only the magic number of the
 * matrices and the block type will be set to the right value. All other
 * matrix parameters have to be set afterwards (e.g. using matalloc() ).
 *
 * \param[out] M Pointer to array of matrices.
 * \param length Number of matrices in the array.
 * \return Pointer to the matrix array.
 * \retval \c NULL if function is unsuccessful.
 */
mat matarralloc(mat M, size_t length)
{
  int mat_ch;
  size_t i_mat;

  if( (mat_ch = matcheck(M)) < 0)
  {

    /* Check the validity of the pointer M and abort if not valid */
    ERROR_MSG("Invalid pointer \n");
    ERROR_RETURN(NULL);
  }
  else if(mat_ch > 0)
  {
    /* If pointer M is valid, check if the existing array matches with
     * the input parameter length.
     */
    for(i_mat = 0;
        ( (M+i_mat)->mag_no == MATRIX) &&
        ( (M+i_mat)->blk_type == BLK_ARRAY);
        i_mat ++)
    {;}
    if ( (i_mat == length)  &&  ( (M+i_mat)->mag_no == MATRIX) &&
         ((M+i_mat)->blk_type == BLK_END))
    {
      /* return M, if matching */
      return(M);
    }
    else
    {
      /* free attached memory (including terminator), if M is not matching */
      for(i_mat = 0;
          ( (M+i_mat)->mag_no == MATRIX) &&
          ( (M+i_mat)->blk_type == BLK_ARRAY);
          i_mat ++)
      {
        matfree(M+i_mat);
      }
      matfree(M+i_mat);
    }
  } /* else if mat_ch > 0 */

  /* If pointer M is NULL (mat_ch = 0) or valid but the array length did
   * not match, allocate new array.
   */
  CONTROL_MSG(CONTROL, "create new matrix array of length %d\n", length);

  CLEED_ALLOC_CHECK(M = (mat)calloc((length+1), sizeof(struct mat_str)));

  /* Set magic numbers and blk_type for the array elements (and reset r/iel) */
  for(i_mat = 0; i_mat < length; i_mat ++)
  {
    (M+i_mat)->mag_no = MATRIX;
    (M+i_mat)->blk_type = BLK_ARRAY;
    (M+i_mat)->rel = (M+i_mat)->iel = NULL;
  }
 
  /* Set magic numbers and blk_type for the terminator */
  (M+length)->mag_no = MATRIX;
  (M+length)->blk_type = BLK_END;
  (M+i_mat)->rel = (M+i_mat)->iel = NULL;

  return(M);
} /* end of function matarralloc */
