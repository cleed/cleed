/*********************************************************************
 *                    TEST_FTS_RM_NEG_DATA.C
 *
 *  Copyright 2014-2015 Liam Deacon <liam.m.deacon@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later.
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 *
 *********************************************************************/

/*!
 * \file
 * \brief Unit tests for fts_rm_neg_data() function.
 */

#include "minunit.h"
#include "ftsmooth.h"
