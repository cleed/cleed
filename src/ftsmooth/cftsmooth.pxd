cdef extern from "ftsmooth.h":
    const char* __version__ "FTSMOOTH_VERSION"
    const char* __description__ "FTSMOOTH_SHORTDESC"
    const char* __copyright__ "FTSMOOTH_COPYRIGHT"
    const char* __maintainer__ "FTSMOOTH_MAINTAINER"
    const char* __license__ "FTSMOOTH_LICENSE"
    const char* __author__ "FTSMOOTH_AUTHOR"
    