/*********************************************************************
 *                       IV.H
 *
 *  Copyright 1992-2014 Georg Held <g.held@reading.ac.uk>
 *
 *  Licensed under GNU General Public License 3.0 or later.
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 *********************************************************************/

/*! \file
 *
 * Header
 */

#ifndef _iv_h_
#define _iv_h_

#define    UP       200.0       /*!< forground intensity of pixels */
#define    LOW        0.0       /*!< background intensity of pixels */
#define    HSIZE    250         /*!< horizontal size of image blocks */
#define    VSIZE    220         /*!< vertical size of image blocks */
#define    HORIG     30         /*!< horizontal origin of coordinate */

/* system within an image block */
#define    VORIG     15         /*!< vertical origin of coordinate */

/* system within an image block */
#define    HLENGTH  200         /*!< length of energy axis in pixels */
#define    VLENGTH  200         /*!< length of intensity axis in pixels */

#endif /* _iv_h_ */
