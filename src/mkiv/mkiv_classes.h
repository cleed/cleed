/*********************************************************************
 *													mkiv_classes.h
 *
 *  Copyright (C) 2013-2014 Liam Deacon <liam.m.deacon@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 *
 * Changes:
 *   LD/18 Nov 2014 - creation
 *********************************************************************/

/*!
 * \file mkiv_classes.h
 * \author Liam Deacon
 * \date 18 Nov 2014
 */


#ifndef __mkiv_classes_H__
#define __mkiv_classes_H__

#ifdef __cplusplus__ /* if this is a C++ compiler then use C linkage */
extern C {
#endif /* __cplusplus__ */

/* includes */


/* defines */


/* enums, structs and typedefs */


/* functions */



#ifdef __cplusplus__ /* if this is a C++ compiler then use C linkage */
namespace cleed {

/* classes */

} /* namespace cleed */

}
#endif /* __cplusplus__ */

#endif /* __mkiv_classes_H__ */
